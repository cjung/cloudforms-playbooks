# README

This is a collection of Playbooks particularly designed to be useful in the context of ManageIQ or CloudForms.

* installpackage: install a user specified package into the VM
* ocp-create-project: using the URI module to create a project in OpenShift (didn't work see below)
* ocp-create-project-via-shell: second approach by just calling curl
* operatorcommand: runn a user specified command in the VM
* restartservice: restart the user specified service in the VM
* selinux-enforcing: set SELinux to enforcing mode if not already done. More details can be found in the blog post [Enforce SELinux Compliance Policy with Ansible](http://www.jung-christian.de/post/2017/12/enforce-selinux-with-ansible/)
